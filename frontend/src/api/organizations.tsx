import { BASE_API } from '.';
import { OrganizationRaw } from '../components/Interfaces';

export const getOrganizations = async () => {
  try {
    const request = await fetch(`${BASE_API}/organizations`);
    const response = await request.json();
    const data = response.data;
    return data;
  } catch (error) {
    console.log('Error getting organizations', error);
    return false;
  }
};

export const getOrganization = async (options: { slug: string }) => {
  try {
    const request = await fetch(`${BASE_API}/organizations/${options.slug}`);
    const response = await request.json();
    const data = response.data;
    return data;
  } catch (error) {
    console.log('Error getting one organization', error);
    return false;
  }
};

export const postOrganization = async (options: { organization: OrganizationRaw; csrf: string }) => {
  try {
    await fetch(`${BASE_API}/organizations`, {
      method: 'POST',
      mode: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'x-csrf-token': options.csrf,
      },
      body: JSON.stringify(options.organization),
    });
  } catch (error) {
    console.log('Error posting a organization', error);
    return false;
  }
};

// What is the purpose of thie code ???
async function getAllOrganizations() {
  return new Promise((resolve, reject) => {
    fetch('http://localhost:3000/organizations/')
      .then(res => {
        return res.json();
      })
      .then(res => {
        /*                console.log(res); */
        resolve(res);
      })
      .catch(error => {
        reject(error);
      });
  });
}
export default getAllOrganizations;
// I dont why so i dont gonna delete it
