import { BASE_API } from '.';

export const getCsrf = async () => {
  try {
    const request = await fetch(`${BASE_API}/auth/csrf`);
    const response = await request.json();
    const data = response.data;
    return data.csrf;
  } catch (error) {
    console.log('Error getting csrf', error);
  }
};

export const postTest = async () => {
  const csrf = await getCsrf();
  try {
    const request = await fetch(`${BASE_API}/test`, {
      method: 'POST',
      credentials: 'include',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'X-Csrf-Token': csrf,
      },
      body: JSON.stringify({}),
    });
    const response = await request.json();
    const data = response.data;
    return data.csrf;
  } catch (error) {
    console.log('Error getting csrf', error);
  }
};
