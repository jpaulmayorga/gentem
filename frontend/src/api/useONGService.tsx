import { useEffect, useState } from 'react';
import { ONG } from '../components/Interfaces/ONG';
import { Service } from '../components/Interfaces/Service';

export interface ONGS {
  results: ONG[];
}

const api = 'http://localhost:3000/organizations/';

function useONGService() {
  const [result, setResult] = useState<Service<ONG>>({ status: 'loading' });

  useEffect(() => {
    fetch(`${api}`)
      .then(response => response.json())
      .then(response => setResult({ status: 'loaded', payload: response }))
      .catch(error => setResult({ status: 'error', error }));
  }, []);
  return result;
}

export default useONGService;

export function useONGID(id: any) {
  const [result, setResult] = useState<Service<ONG>>({ status: 'loading' });

  useEffect(() => {
    fetch(`${api}${id}`)
      .then(response => response.json())
      .then(response => setResult({ status: 'loaded', payload: response }))
      .catch(error => setResult({ status: 'error', error }));
  }, [id]);
  return result;
}
