import React from 'react';
import { Link } from 'react-router-dom';
import { Layout, Alert } from 'antd';
import Logo from '../assets/svg/Logo';
import Heart from '../assets/svg/heart';
import '../less/Header.less';

function Header() {
  return (
    <>
      <div className='layout-center fixed bg '>
        <Layout className='headerContent layout'>
          <div className='header'>
            <div className='header__logo'>
              <Link to='/'>
                <Logo />
              </Link>
            </div>
            <div>
              <a href='https://gentem.typeform.com/to/qN1hfz' rel='noopener noreferrer' target={'_blank'}>
                <div className='header__btn'>
                  <span className='header__btn-text'>
                    Únete a gentem
                    <Heart />
                  </span>
                </div>
              </a>
            </div>
          </div>
        </Layout>
      </div>
      <div className='construction'>
        <Alert
          message='Aviso'
          description='El sitio está en construcción, algunas funciones pueden no estar activadas.'
          type='info'
          showIcon
          closable
        />
      </div>
    </>
  );
}

export default Header;
