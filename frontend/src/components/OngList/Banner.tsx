import React from 'react';

function Banner() {
  return (
    <div className='Banner'>
      <div className='Banner__content'>
        <div className='Banner__content--title'>
          <h2>Encuentra y dona</h2>
        </div>
        <div className='Banner__content--subtitle'>
          <h5>Encuentra la fundación, ONG o proyecto al que quieres apoyar</h5>
        </div>
      </div>
    </div>
  );
}

export default Banner;
