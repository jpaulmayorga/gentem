import { Layout, Modal } from 'antd';
import React, { useState } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { ModalContent } from './Contenido';
import TargetBase from '../Usables/TargetBase';
import ResultItem from './ResultItem';

const { Sider } = Layout;

function ContenidoSider(props: any) {
  const [estado, setEstado] = useState({
    visible: false,
  });
  const filter = props.filter;
  const handleModal = () => {
    setEstado({ visible: true });
  };
  const handleClose = () => {
    setEstado({ visible: false });
  };

  /* const sanate = (string: any) => {
    let content = string.replace(/ /g, '-');
    return content;
  }; */
  const ONG = props.data;
  return (
    <Sider className='OngList__Result'>
      <div className='OngList__Result--quantity'>
        <h6>Mostrando {ONG.length} proyectos</h6>
        <div className='quantity-btn' onClick={handleModal}>
          FILTROS
        </div>
        <Modal maskClosable={true} footer={null} centered={true} onCancel={handleClose} visible={estado.visible}>
          <ModalContent />
        </Modal>
      </div>
      {filter && <TargetBase filter={filter} />}
      {ONG.map((ong: any) => {
        return (
          <Link
            key={ong.slug}
            to={{
              pathname: `/org/${ong.slug}`,
              state: {
                ong: ong,
              },
            }}>
            <ResultItem name={ong.name} desc={ong.description} location={'Bogotá, Colombia'} />
          </Link>
        );
      })}
    </Sider>
  );
}
export default withRouter(ContenidoSider);
