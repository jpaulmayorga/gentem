import React from 'react';
import TargetBase from '../Usables/TargetBase';
import Logo from '../../assets/img/logoDefault.png';

function ResultItem(props: any) {
  const array = ['Adolescentes'];

  return (
    <div className='OngList__Result--itemContainer'>
      <div className='Result__item'>
        <div className='Result__item--img'>
          <img src={props.img || Logo} alt='ONG Logo' />
        </div>
        <div className='Result__item--details'>
          <div className='item__details--title'>
            <div className='title-child'>
              <h4>{props.name}</h4>
            </div>
          </div>
          <div className='item__details--desc'>
            <h6>{props.desc}</h6>
          </div>
          <div className='item__details--footer'>
            <div className='details__footer--target'>
              <div className='footer__target--principal'>
                <TargetBase array={array} filter={0} />
              </div>
              {/*  <div className='footer__target--more'> +1</div> */}
            </div>
            <div className='details__footer--location'>{props.location}</div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default ResultItem;
