import { Layout, Divider, Button } from 'antd';
import React from 'react';
import Logo from '../../assets/img/logoDefault.png';

const { Sider } = Layout;
function ContenidoSider(props: any) {
  return (
    <Sider className='OngProfile__HowToDonate'>
      <ResponsiveDonate
        logo={props.logo}
        name={props.name}
        instructionstodeliverproducts={props.instructionstodeliverproducts}
        accounts={props.accounts}
        paymentslink={props.paymentslink}
      />
    </Sider>
  );
}
export default ContenidoSider;

export function ResponsiveDonate({ instructionstodeliverproducts, logo, name, accounts, paymentslink }: any) {
  return (
    <React.Fragment>
      <div className='OngProfile__UserModal'>
        <div className='OngProfile__UserModal--Img'>
          <img src={logo || Logo} alt='Logo de ONG' />
        </div>
        <div>
          <span className='OngProfile__UserModal--Title'>{name}</span>
        </div>
      </div>
      <div className='OngProfile__HowToDonate--Title'>
        <h3>¿Cómo donar?</h3>
      </div>
      <div className='OngProfile__HowToDonate--Donate'>
        {paymentslink && (
          <>
            <h4 className='OngProfile__Donate--Title h4'> Dona Online </h4>
            <Divider className='Divider' />
            <a href={paymentslink} target='_blank' rel='noopener noreferrer'>
              <Button type='primary' className='OngProfile__Donate--Button'>
                Haz tu donación online
              </Button>
            </a>
          </>
        )}
        {accounts && (
          <div className='OngProfile__Donate--DonateWay'>
            <h4 className='OngProfile__DonateWay--Bank h4'>Consigna a una cuenta bancaria</h4>
            <Divider className='Divider' />
            <span className='OngProfile__DonateWay--BankID span'>{accounts}</span>
          </div>
        )}
      </div>
      {instructionstodeliverproducts && (
        <div className='OngProfile__HowToDonate--DonateProduct'>
          <h4 className='OngProfile__DonateProduct--Title h4'> Haz una donación de productos </h4>
          <Divider className='Divider' />
          <div className='OngProfile__DonateProduct--DonateWay'>
            <span className='span'>{instructionstodeliverproducts}</span>
          </div>
        </div>
      )}
    </React.Fragment>
  );
}
