import { Organization } from '../../Interfaces';
import { Card } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';

interface Props {
  organization: Organization;
}

export function OrganizationItem({ organization }: Props) {
  return (
    <Link to={`/organizationsTest/${organization.organizationId}`}>
      <Card cover={<img src={organization.logo} alt='Logo' />}>
        <h2>{organization.name}</h2>
        <p>{organization.objective}</p>
      </Card>
    </Link>
  );
}
