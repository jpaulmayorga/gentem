import { Layout, Divider, Button, Modal } from 'antd';
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { ResponsiveDonate } from './CotenidoSider';
import { MessageOutlined } from '@ant-design/icons';
import Logo from '../../assets/img/logoDefault.png';
import cat from '../../api/categories.json';

import Facebook from '../../assets/img/fb.svg';
import Instagram from '../../assets/img/insta.svg';
import Whatsapp from '../../assets/img/whatsapp.svg';
import Location from '../../assets/img/location.svg';
import TargetBase from '../Usables/TargetBase';

const { Content } = Layout;

function Contenido(props: any) {
  const [estado, setEstado] = useState<any>({
    visible: false,
  });
  const [communityArray, setCommunityArray] = useState<any>([]);
  const handleModal = () => {
    setEstado({ visible: true });
  };
  const handleClose = () => {
    setEstado({ visible: false });
  };
  const communityWorkWith = props.communityworkwith;
  const catdata = cat.data;
  useEffect(() => {
    catdata.map((target: any) => {
      return communityWorkWith.map((community: any) => {
        return target.cat_id.some((iqual: number) => iqual === community)
          ? setCommunityArray((communityArray: []) => [...communityArray, target.cat_name])
          : false;
      });
    });
  }, [catdata, communityWorkWith]);
  const filter = 0;
  return (
    <Content className='OngProfile__Hero'>
      <div className='OngProfile__Hero--User'>
        <div className='OngProfile__User--ImgBreadcrumb'>
          <div className='OngProfile__User--Img'>
            <img src={props.logo || Logo} alt='User 1' />
          </div>
          <div className='OngProfile__User--Breadcrumb'>
            <Link to='/projects'>Proyectos</Link> / {props.name}
          </div>
        </div>
        <div className='OngProfile__User--Info'>
          <h2 className='OngProfile__Info--Title '>{props.name}</h2>
          <TargetBase array={communityArray} filter={filter} />

          <h3 className='OngProfile__Info--Description'>{props.description}</h3>
        </div>
        <div className='OngProfile__User--Objective'>
          <p className='OngProfile__objective--Description'>{props.objetive}</p>
        </div>
        <div className='OngProfile__User--HowUseDonation'>
          <h3 className='OngProfile__HowUseDonation--Title'>¿Cómo usamos las donaciones que recibimos?</h3>
          <p className='OngProfile__HowUseDonation--Description'>{props.howusedonation}</p>
        </div>
        {props.sponsors && (
          <div className='OngProfile__User--Sponsors'>
            <h4 className='OngProfile__Sponsors--Title'>Patrocinadores</h4>
            <Divider className='Divider' />
            <div className='OngProfile__Sponsors--Container'>
              {props.sponsors.map((sponsor: any) => (
                <div className='OngProfile__Container--IMG'>
                  <img src={sponsor} alt='Logo sponsor' />
                </div>
              ))}
            </div>
          </div>
        )}

        {(props.website || props.email || props.phone) && (
          <div className='OngProfile__HowToDonate--Contact'>
            <h4 className='OngProfile__Contact--Title'> Contacto </h4>
            <Divider className='Divider' />
            <div className='OngProfile__Contact--Info'>
              {props.website && (
                <div className='OngProfile__Info-Web'>
                  <strong>Website</strong> <br />
                  <span>
                    <a href={props.website} rel='noopener noreferrer' target='_blank'>
                      {props.website}
                    </a>
                  </span>
                </div>
              )}
              {props.email && (
                <div className='OngProfile__Info-Email'>
                  <strong>Email</strong> <br />
                  <span>
                    <a href={`mailto://${props.email}`}>{props.email}</a>
                  </span>
                </div>
              )}
              {props.phone && (
                <div className='OngProfile__Info-Tel'>
                  <strong>Teléfono</strong> <br />
                  <span>{props.phone}</span>
                </div>
              )}
            </div>
          </div>
        )}

        {(props.facebook || props.instagram || props.whatsapp) && (
          <div className='OngProfile__Social'>
            <h4 className='OngProfile__Social--Title'>Redes sociales</h4>
            <Divider className='Divider' />
            <div className='OngProfile__Social--Networks'>
              {props.whatsapp && (
                <a
                  href={`https://api.whatsapp.com/send?phone=${props.whatsapp}`}
                  rel='noopener noreferrer'
                  target='_blank'>
                  <img src={Whatsapp} alt='whatsapp logo' />
                </a>
              )}
              {props.instagram && (
                <a href={props.instagram} rel='noopener noreferrer' target='_blank'>
                  <img src={Instagram} alt='instagram logo' />
                </a>
              )}
              {props.facebook && (
                <a href={props.facebook} rel='noopener noreferrer' target='_blank'>
                  <img src={Facebook} alt='facebook logo' />
                </a>
              )}
            </div>
          </div>
        )}

        <div className='OngProfile__User--Address'>
          <h4 className='OngProfile__Address-Title'> Área de trabajo </h4>
          <Divider className='Divider' />
          <div className='OngProfile__Address--Info'>
            <div className='OngProfile__Info--Icon'>
              <img src={Location} alt='location icon' />
            </div>
            <div className='OngProfile__Info--Data'>
              <span>
                {props.location.municipal}&nbsp;
                {props.location.region}&nbsp;
                {props.location.country}
              </span>
            </div>
          </div>
        </div>
        <div className='OngProfile__FixedHowToDonate'>
          <div>
            <Button type='primary' className='OngProfile__FixedHowToDonate--Button' onClick={handleModal}>
              ¿Cómo donar?
            </Button>
            <Modal maskClosable={true} footer={null} centered={true} onCancel={handleClose} visible={estado.visible}>
              <div className='Modal__Active'>
                <ResponsiveDonate
                  logo={props.logo || Logo}
                  paymentslink={props.paymentslink}
                  name={props.name}
                  instructionstodeliverproducts={props.instructionstodeliverproducts}
                  accounts={props.accounts}
                />
              </div>
            </Modal>
            <Button type='primary' className='OngProfile__FixedHowToDonate--MessageButton'>
              <MessageOutlined />
            </Button>
          </div>
        </div>
      </div>
    </Content>
  );
}
export default Contenido;
