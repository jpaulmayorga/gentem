import React, { useState, useEffect } from 'react';

//import GoogleMapReact from 'google-map-react';

function Map(props: any) {
  /*  const geometryY = parseFloat(props.coordenates.props.ygriega); */
  /*   const geometryX = parseFloat(props.coordenates.props.equis); */
  const [, /* zoom */ setZoom] = useState(props.coordenates.props.zoom);

  useEffect(() => {
    setZoom(props.coordenates.props.zoom);
  }, [props.coordenates.props.zoom]);

  return (
    <div className='Map'>{/* <GoogleMap defaultZoom={10} defaultCenter={{ lat: geometryY, lng: geometryX }} /> */}</div>
  );
}

export default Map;

export function Marcador(props: any) {
  const { color, name } = props;

  const conditional = () => {
    if (props.municipal && props.region) {
      return ', ';
    } else {
      return false;
    }
  };
  return (
    <>
      <div className='marker' style={{ backgroundColor: color, cursor: 'pointer' }} title={name} />
      <div className='mapInfo'>
        <div className='marker'>
          <div className='marker__fill'></div>
          <div className='marker__fill--fill'></div>
        </div>
        <div className='address'>
          <div className='textContainer'>
            <div className='textContainer__city'>
              {props.municipal && props.municipal}
              {conditional()}
              {props.region && props.region}
            </div>
            <div className='textContainer__country'>{props.pais}</div>
          </div>
        </div>
      </div>
    </>
  );
}

export const styles = [
  {
    elementType: 'geometry',
    stylers: [
      {
        color: '#f5f5f5',
      },
    ],
  },
  {
    elementType: 'labels.icon',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
  {
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#616161',
      },
    ],
  },
  {
    elementType: 'labels.text.stroke',
    stylers: [
      {
        color: '#f5f5f5',
      },
    ],
  },
  {
    featureType: 'administrative.land_parcel',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#bdbdbd',
      },
    ],
  },
  {
    featureType: 'poi',
    elementType: 'geometry',
    stylers: [
      {
        color: '#eeeeee',
      },
    ],
  },
  {
    featureType: 'poi',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#757575',
      },
    ],
  },
  {
    featureType: 'poi.park',
    elementType: 'geometry',
    stylers: [
      {
        color: '#e5e5e5',
      },
    ],
  },
  {
    featureType: 'poi.park',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#9e9e9e',
      },
    ],
  },
  {
    featureType: 'road',
    elementType: 'geometry',
    stylers: [
      {
        color: '#ffffff',
      },
    ],
  },
  {
    featureType: 'road.arterial',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#757575',
      },
    ],
  },
  {
    featureType: 'road.highway',
    elementType: 'geometry',
    stylers: [
      {
        color: '#dadada',
      },
    ],
  },
  {
    featureType: 'road.highway',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#616161',
      },
    ],
  },
  {
    featureType: 'road.local',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#9e9e9e',
      },
    ],
  },
  {
    featureType: 'transit.line',
    elementType: 'geometry',
    stylers: [
      {
        color: '#e5e5e5',
      },
    ],
  },
  {
    featureType: 'transit.station',
    elementType: 'geometry',
    stylers: [
      {
        color: '#eeeeee',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'geometry',
    stylers: [
      {
        color: '#c9c9c9',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#9e9e9e',
      },
    ],
  },
];
