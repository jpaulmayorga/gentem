import React from 'react';
import '../../less/Loader.less';

function Loader() {
  return (
    <>
      {window.scroll(0, 0)}
      <div className='loader'></div>
    </>
  );
}
export default Loader;
