import React from 'react';
import Target from './Target';
import '../../less/lib/_usables.less';

function TargetBase(props: any) {
  const result = props.array;
  const filter = props.filter;

  if (filter !== 0) {
    return (
      <div className='Target'>
        <div className='Target-grid'>
          {filter.map((title: any) => {
            return <Target key={title} filtro={title} />;
          })}
        </div>
      </div>
    );
  }
  return (
    <div className='Target'>
      <div className='Target-grid'>
        {result.map((title: any) => {
          return <Target key={title} desc={title} />;
        })}
      </div>
    </div>
  );
}
export default TargetBase;
