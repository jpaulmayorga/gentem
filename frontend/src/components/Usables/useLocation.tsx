import { useEffect, useState } from 'react';
import Geocode from 'react-geocode';

function useLocation(props: any) {
  Geocode.setApiKey('AIzaSyDbIWuop2EbMbV01G3344p2e6zIjAnjFwo');
  const [mapLocation, setMapLocation] = useState({
    municipal: '',
    region: '',
    country: '',
  });

  const [coord, setCoord] = useState({
    y: '',
    x: '',
    zoom: 1,
  });
  const [pais, setPais] = useState('');
  const [region, setRegion] = useState('');
  const [municipalidad, setMunicipalidad] = useState('');
  const [url, setUrl] = useState<any>();

  useEffect(() => {
    setUrl(props);
    if (url === undefined) {
      setMapLocation({ municipal: '', region: 'Desde LATAM', country: 'Hacia el mundo' });
      setCoord({ y: '-26.231718', x: '-63.2485696', zoom: 0 });
    } else if (url !== undefined) {
      const cadena = url.indexOf('@');
      const cortado = url.slice(cadena + 1);
      const coma = cortado.indexOf(',');
      const ygriega = cortado.slice(0, coma);
      const cadena2: number = ygriega.length;
      const corta2 = url.slice(cadena2 + cadena + 2);
      const equis = corta2.slice(0, coma);
      setCoord({ y: ygriega, x: equis, zoom: 12 });
      Geocode.fromLatLng(coord.y, coord.x).then(
        (response: any) => {
          const results = response.results;

          results.map((locality: any) => {
            if (locality.types.includes('country')) {
              let Pais = locality.formatted_address;
              setPais(Pais);
            } else if (locality.types.includes('administrative_area_level_1')) {
              let coma = locality.formatted_address.indexOf(',');
              let Municipalidad = locality.formatted_address.slice(0, coma);
              setMunicipalidad(Municipalidad);
            } else if (locality.types.includes('administrative_area_level_2')) {
              let coma = locality.formatted_address.indexOf(',');
              let Region = locality.formatted_address.slice(0, coma);
              setRegion(Region);
            }
            return setMapLocation({ municipal: municipalidad, region: region, country: pais });
          });
        },
        (error: any) => {
          /* console.error(error); */
        },
      );
    }
  }, [props, pais, municipalidad, region, url, coord.y, coord.x]);

  return { props: { mapLocation: mapLocation, ygriega: coord.y, equis: coord.x, zoom: coord.zoom } };
}

export default useLocation;
