import React, { useState } from 'react';
import Close from '../../assets/svg/close';

function Target(props: any) {
  const [estado, setEstado] = useState({
    display: 'flex',
  });
  const handleClose = () => {
    setEstado({ display: 'none' });
  };

  if (props.filtro) {
    return (
      <React.Fragment>
        <span className='Target-span' style={estado}>
          {props.filtro}
          <span className='Target__btn' onClick={handleClose}>
            <Close />
          </span>
        </span>
      </React.Fragment>
    );
  }

  return (
    <React.Fragment>
      <span className='Target-span'>{props.desc} </span>
    </React.Fragment>
  );
}
export default Target;
