export interface ONG {
  name: string;
  id: any;
  description: string;
  logo: string;
  data: any;
}
