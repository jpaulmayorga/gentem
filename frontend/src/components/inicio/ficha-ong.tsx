import React, { useEffect, useState } from 'react';
import i1 from '../../assets/img/ongs/1.png';
import i2 from '../../assets/img/ongs/2.svg';
import i3 from '../../assets/img/ongs/3.png';
import i4 from '../../assets/img/ongs/4.png';
import i5 from '../../assets/img/ongs/5.png';
import i6 from '../../assets/img/ongs/6.png';
import i7 from '../../assets/img/ongs/7.png';
import i8 from '../../assets/img/ongs/8.png';

function OngFicha(props: any) {
  const [dat, setDat] = useState<any>(props.datos);

  useEffect(() => {
    if (props.loading === false) {
    }
  }, []);
  console.log('ficha', dat);
  return (
    <div className='container-fichas'>
      <div className='content'>
        <div className='i4'>
          <div className='colm-4'>
            <Ficha i={i1} />
          </div>
          <div className='colm-4'>
            <Ficha i={i2} />
          </div>
          <div className='colm-4'>
            <Ficha i={i3} />
          </div>
          <div className='colm-4'>
            <Ficha i={i4} />
          </div>
        </div>

        <div className='i4'>
          <div className='colm-4'>
            <Ficha i={i5} />
          </div>
          <div className='colm-4'>
            <Ficha i={i6} />
          </div>
          <div className='colm-4'>
            <Ficha i={i7} />
          </div>
          <div className='colm-4'>
            <Ficha i={i8} />
          </div>
        </div>
      </div>
    </div>
  );
}
function Ficha(props: any) {
  return (
    <div className='ficha'>
      <div className='img'>
        <img src={props.i} alt='' />
      </div>
    </div>
  );
}
export default OngFicha;
