import React from 'react';

function ResCov(props: any) {
  return (
    <div className='resCov'>
      <div className='valor'>
        <span>{props.res}</span>
      </div>
      <div className='result-info'>
        <span>{props.info}</span>
      </div>
    </div>
  );
}
export default ResCov;
