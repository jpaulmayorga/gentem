import React, { Fragment } from 'react';
/* import { url } from 'inspector';
import i1 from '../../assets/img/1.png'; */
function OvalI(props: any) {
  return (
    <div className='container-img-info'>
      <div className='cont'>
        <div className='formaIm'>
          <img src={props.i} alt='' />
        </div>
      </div>
    </div>
  );
}
function Forma(props: any) {
  return (
    <Fragment>
      <div className='container-img-info'>
        <div className='cont'>
          <div className='forma'>
            <Forma color={props.col} />
          </div>
          <div className='img'>
            <ImgForma img={props.i} id={props.id} />
          </div>
        </div>
      </div>
      <svg className='svgF' viewBox='0 0 411 386' fill='none' xmlns='http://www.w3.org/2000/svg'>
        <path
          d='M211.296 5.8496C361.84 22.1983 405.93 101.067 409.156 138.457C411.417 152.074 413.252 189.163 402.51 228.587C389.083 277.867 281.113 462.322 160.994 349.176C40.8758 236.029 8.02004 114.416 1.13265 72.9279C-5.75474 31.4398 23.1151 -14.5862 211.296 5.8496Z'
          fill={props.color}
        />
      </svg>
    </Fragment>
  );
}
function ImgForma(props: any) {
  var id: string = 'image' + props.id;
  var m: string = '';
  var i: string = 'pattern' + id;
  console.log('#' + id);
  return (
    <svg
      width='600'
      height='505'
      viewBox='0 0 600 505'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
      xmlnsXlink='http://www.w3.org/1999/xlink'>
      <mask id={(m = 'mask' + id)} mask-type='alpha' maskUnits='userSpaceOnUse' x='26' y='71' width='489' height='325'>
        <path
          d='M129.831 143.3C239.317 48.6976 338.889 67.5369 374.989 88.7818C388.818 95.7624 423.769 117.787 452.945 150.041C489.416 190.357 589.665 381.467 411.088 394.055C232.51 406.643 101.045 353.121 58.9107 331.852C16.7765 310.582 -7.0264 261.552 129.831 143.3Z'
          fill='#F44B53'
        />
      </mask>
      <g mask={'url(#' + m + ')'}>
        <rect x='27.8577' y='13.6514' width='557.145' height='384.411' fill='#C4C4C4' />
        <rect x='27.8577' y='13.6514' width='557.145' height='384.411' fill={'url(#' + i + ')'} />
      </g>
      <defs>
        <pattern id={i} patternContentUnits='objectBoundingBox' width='1' height='1'>
          <use xlinkHref={'#' + id} transform='translate(-0.0170507) scale(0.000847624 0.0012285)' />
        </pattern>
        <image id={id} width='1220' height='814' xlinkHref={props.img} />
      </defs>
    </svg>
  );
}
export default OvalI;
