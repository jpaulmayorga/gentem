import React from 'react';

function Col(props: any) {
  return (
    <div className='container-colabor'>
      <div className='colaborar'>
        <div className='texto'>
          <h4>{props.desc}</h4>
        </div>
        <div className='btn-fl'>
          <button className='btn-colab' style={{ background: props.colorBtn }}>
            <span>{props.btn}</span>
          </button>
        </div>
      </div>
    </div>
  );
}
export default Col;
