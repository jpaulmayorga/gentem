import React from 'react';
import { Link } from 'react-router-dom';
function Banner() {
  return (
    <div className='container-banner'>
      <div className='img-banner-I'></div>
      <div className='content-banner'>
        <div className='content'>
          <div className='text'>
            <span>
              Ayuda a proyectos sociales que están trabajando con comunidades afectadas por el coronavirus en América
              Latina
            </span>
          </div>
          <div className='btn-banner'>
            <Link to='/projects'>
              <button className='btn-home'>
                <span>Ver todos los proyectos</span>
              </button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Banner;
