import React from 'react';
import Heart from '../../assets/svg/heart';
/* import manos from '../../assets/img/manos.png'; */
function Cont() {
  return (
    <div className='container-contribuir'>
      <div className='img'></div>
      <div className='content'>
        <div className='texto'>
          <span>¿Eres una fundación, ONG o proyecto de carácter social?</span>
          <p>Haz parte de gentem y mejora tus probabilidades de recibir donaciones.</p>
        </div>
        <div className='btn-fl'>
          <button className='btn-contribuir' style={{ background: '#237ed5' }}>
            <span>Regístrate </span>
            <Heart />
          </button>
        </div>
      </div>
    </div>
  );
}
export default Cont;
