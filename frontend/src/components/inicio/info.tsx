import React from 'react';
function Info(props: any) {
  return (
    <div className='container-info'>
      <div className='texto'>
        <h2>{props.tit}</h2>
      </div>
      <div className='linea' style={{ background: props.col }}></div>
      <div className='desc'>
        <p>{props.cuerpo}</p>
      </div>
    </div>
  );
}
export default Info;
