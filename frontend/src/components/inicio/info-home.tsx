import React from 'react';
import FlDos from './fl-2';
import OvalI from './oval-img';
import Info from './info';
import img1 from '../../assets/img/poverty.png';
import img2 from '../../assets/img/real.png';
import img3 from '../../assets/img/together.png';
function Infohome() {
  return (
    <div className='container-info-home'>
      <FlDos>
        <div className='info-home'>
          <div className='colm-2'>
            <OvalI col='#FFCF53' i={img1} id='1' />
          </div>
          <div className='colm-2'>
            <Info
              col='#FFCF53'
              tit='La extrema 
pobreza amenaza Latino América'
              cuerpo='En los países latinos más del 50% de la población trabaja en condiciones de informalidad, sin ningún tipo de protección de parte del gobierno. Cada día que no pueden trabajar es un día que pasan hambre.'
            />
          </div>
        </div>
      </FlDos>
      <FlDos>
        <div className='flex-rev'>
          <div className='colm-2'>
            <Info
              col='#1E8A61'
              tit='Unidos somos 
más fuertes'
              cuerpo='Con la ayuda de cada uno de nosotros, por pequeña que sea, podemos darle a alguien un respiro en estos tiempos de incertidumbre y la esperanza de un manaña mejor.'
            />
          </div>
          <div className='colm-2'>
            <OvalI col='#FFCF53' i={img3} id='2' />
          </div>
        </div>
      </FlDos>
      <FlDos>
        <div className='info-home'>
          <div className='colm-2'>
            <OvalI col='#FFCF53' i={img2} id='3' />
          </div>
          <div className='colm-2'>
            <Info
              col='#237ED5'
              tit='Iniciativas 
sociales reales'
              cuerpo='Queremos que tu ayuda llegue a la población que realmente quieres impactar, por eso las fundaciones que encontrás en gentem han sido verificadas por nuestro equipo.'
            />
          </div>
        </div>
      </FlDos>
    </div>
  );
}
export default Infohome;
