import React from 'react';
import { Layout } from 'antd';
import Facebook from '../assets/img/fb.svg';
import Instagram from '../assets/img/insta.svg';
import LinkedIn from '../assets/img/linkedIn.svg';

import '../less/Footer.less';

function Footer() {
  return (
    <Layout>
      <Layout.Footer className='footer'>
        <div className='social-media'>
          <div>
            <a href='https://www.facebook.com/gentemorg' target='_blank' rel='noopener noreferrer'>
              <img className='social-media__Icon' src={Facebook} alt='facebook logo' />
            </a>
          </div>
          <div>
            <a href='https://www.instagram.com/gentem_org/' target='_blank' rel='noopener noreferrer'>
              <img className='social-media__Icon' src={Instagram} alt='instagram logo' />
            </a>
          </div>
          <div>
            <a href='https://www.linkedin.com/company/gentem-org/' target='_blank' rel='noopener noreferrer'>
              <img className='social-media__Icon' src={LinkedIn} alt='linkedin logo' />
            </a>
          </div>
        </div>
        <div className='footer__contact'>
          <span>
            ¿Tienes alguna duda? Contactanos en
            <span className='mail-color'>
              <a href='mailto:hola@gentem.org'> hola@gentem.org</a>
            </span>
          </span>
        </div>

        {/* <div className='language'>
            <div>Castellano</div>
            <span className='circle'></span>
            <div className='activate'>Ingles</div>
          </div> */}
      </Layout.Footer>
    </Layout>
  );
}

export default Footer;
