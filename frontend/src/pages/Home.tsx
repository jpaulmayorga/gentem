import React from 'react';
import Contenedor from '../components/inicio/container';
import Banner from '../components/inicio/banner';
import Fl from '../components/inicio/fl-1';
import Stats from '../components/inicio/stats';
import Info from '../components/inicio/info-home';
import Colabor from '../components/inicio/colaboracion';
import Nos from '../components/inicio/nosotros-home';
import Ongs from '../components/inicio/ongs';
import Contribuir from '../components/inicio/contribuir';
import '../less/Home.less';

function Home() {
  return (
    <div className='Home fakeSpace'>
      <Contenedor>
        <Banner />
        <div className='home'>
          <Fl>
            <Colabor
              desc=' Te conectamos con ONGs, fundaciones y otras iniciativas que están ayudando a los más afectados por la
          emergencia del COVID-19'
              btn='Ayuda ahora'
              colorBtn=' #237ed5'
            />
          </Fl>
          <Fl>
            <Stats />
          </Fl>
          <Fl>
            <Info />
          </Fl>
          <Fl>
            <Colabor
              desc=' El aporte que hagas, por pequeño que te parezca, puede significar un día de comida para una familia.'
              btn='Apoya un proyecto'
              colorBtn='#F44B53'
            />
          </Fl>
          <Fl>
            <Nos />
          </Fl>
          <Fl>
            <Ongs />
          </Fl>
        </div>
        <Fl>
          <Contribuir />
        </Fl>
      </Contenedor>
    </div>
  );
}

export { Home };
