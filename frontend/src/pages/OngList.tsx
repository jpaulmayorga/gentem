import { Layout } from 'antd';
import React, { useState, useEffect } from 'react';

import { getOrganizations } from '../api/organizations';
import { Organization } from '../components/Interfaces';
import Banner from '../components/OngList/Banner';
import Contenido from '../components/OngList/Contenido';
import ContenidoSider from '../components/OngList/ContenidoSider';
import Loader from '../components/Usables/Loader';

import '../less/OngList.less';

function OngList() {
  const [select, setSelect] = useState(undefined);
  const filter = ['Niños y Niñas (7-12)'];
  const [organizations, setOrganizations] = useState<Organization[]>([]);
  const [isLoading, setIsloading] = useState<boolean>(true);

  function handleChange(value: any) {
    setSelect(value);
    filter.push(value);
  }

  useEffect(() => {
    getOrganizations().then(data => {
      setOrganizations(data);
      setIsloading(false);
    });
  }, []);

  return (
    <>
      {isLoading && <Loader></Loader>}
      {organizations.length > 0 && (
        <Layout className='OngList'>
          <Banner />
          <div className='layout-center'>
            <Layout className='OngListContent layout'>
              <Contenido select={handleChange} />
              <ContenidoSider data={organizations} filter={filter} select={select} />
            </Layout>
          </div>
        </Layout>
      )}
    </>
  );
}

export { OngList };
