import React, { useEffect, useState } from 'react';
import Contenedor from '../components/inicio/container';
import { getOrganization } from '../api/organizations';
import { Organization } from '../components/Interfaces';
import { useParams } from 'react-router';

export function OrganizationTestOne() {
  const { slug } = useParams();
  const [organization, setOrganization] = useState<Organization>();
  const [isLoading, setIsloading] = useState<boolean>(true);

  useEffect(() => {
    getOrganization({ slug }).then(data => {
      setOrganization(data);
      setIsloading(false);
    });
  }, [slug]);

  return (
    <div className='Home fakeSpace'>
      <Contenedor>
        <div>{isLoading && 'Cargando'}</div>
        <div>{!organization && !isLoading && 'No existe esa organizacion'}</div>
        {organization && (
          <div>
            <img src={organization.logo} alt='Logo' />
            <h2>{organization.name}</h2>
            <p>{organization.objective}</p>
          </div>
        )}
      </Contenedor>
    </div>
  );
}
