import { Layout } from 'antd';
import React, { useState, useEffect } from 'react';
import { getOrganization } from '../api/organizations';
import { Organization } from '../components/Interfaces';
import { useParams } from 'react-router';
import useLocation from '../components/Usables/useLocation';
import Map from '../components/OngProfile/Map';
import Contenido from '../components/OngProfile/Contenido';
import ContenidoSider from '../components/OngProfile/CotenidoSider';
import Loader from '../components/Usables/Loader';

import '../less/OngProfile.less';

function OngProfile(props: any) {
  /* const location = props.location.pathname.slice(5); */
  /* const ongProfile = props.location.state.ong; */
  console.log(props);
  const { slug } = useParams();
  const [organization, setOrganization] = useState<Organization>();
  const [isLoading, setIsloading] = useState<boolean>(true);
  const [orgLocation, setOrgLocation] = useState<any>();

  useEffect(() => {
    getOrganization({ slug }).then(data => {
      setOrganization(data);
      console.log(data);
      setOrgLocation(data.location);
      setIsloading(false);
    });
  }, [slug]);

  const location = useLocation(orgLocation);

  return (
    <>
      {isLoading && <Loader />}
      {organization && (
        <Layout className='OngProfile'>
          {window.scroll(0, 0)}
          <Map
            googleMapURL={'https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCDr1CIiG6Nop7lpjmIbVk8NVC1IjW_oXE'}
            /* containerElement={<div className='Map' />}
            mapElement={<div style={{ height: '100%' }} />}
            loadingElement={<p>Cargando...</p>} */
            coordenates={location}
            location={
              organization.location ||
              'https://www.google.com/maps/place/Fundacion+Social+Perea/@4.5220606,-77.710575,8z/data=!4m12!1m6!3m5!1s0x8e488fcc303fee0d:0xfae9bf9ff467b7cc!2sFundacion+Social+Perea!8m2!3d5.6866706!4d-76.6555895!3m4!1s0x0:0xfae9bf9ff467b7cc!8m2!3d5.6867082!4d-76.6557312?hl=es-MX'
            }>
            {/* <Marcador
              lat={parseFloat(location.props.ygriega)}
              lng={parseFloat(location.props.equis)}
              name='My marker'
              color='blue'
            /> */}
          </Map>
          <div className='layout-center'>
            <Layout className='OngProfileContent layout'>
              <Contenido
                communityworkwith={organization.communityworkwith}
                name={organization.name}
                description={organization.description}
                logo={organization.logo}
                objetive={organization.objective}
                howusedonation={organization.howusedonations}
                website={organization.website}
                email={organization.email}
                phone={organization.phones}
                facebook={organization.facebook}
                instagram={organization.instagram}
                paymentslink={organization.paymentslink}
                whatsapp={organization.whatsapp}
                sponsors={organization.sponsors}
                accounts={organization.accounts}
                instructionstodeliverproducts={organization.instructionstodeliverproducts}
                location={location.props.mapLocation}
              />
              <ContenidoSider
                name={organization.name}
                accounts={organization.accounts}
                instructionstodeliverproducts={organization.instructionstodeliverproducts}
                paymentslink={organization.paymentslink}
              />
            </Layout>
          </div>
        </Layout>
      )}
    </>
  );
}

export default OngProfile;
