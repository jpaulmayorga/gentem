import React, { useState } from 'react';
import Contenedor from '../components/inicio/container';
import { OrganizationRaw } from '../components/Interfaces';
import { Form, Input, Button } from 'antd';
import { getCsrf } from '../api/auth';
import { postOrganization } from '../api/organizations';

export function OrganizationTestCreate() {
  const [isLoading, setIsloading] = useState<boolean>(false);

  const onFinish = async (data: any) => {
    const newOrganization: OrganizationRaw = data;
    setIsloading(true);

    try {
      const csrf = await getCsrf();
      await postOrganization({ organization: newOrganization, csrf });
      setIsloading(false);
      console.log('Organization creada.');
    } catch (error) {
      console.log('error to post org', error);
    }
  };

  return (
    <div className='Home fakeSpace'>
      <Contenedor>
        <div>{isLoading && 'Cargando'}</div>
        <Form onFinish={onFinish}>
          <Form.Item label='name' name='name'>
            <Input />
          </Form.Item>
          <Form.Item label='logo' name='logo'>
            <Input />
          </Form.Item>
          <Form.Item label='objective' name='objective'>
            <Input />
          </Form.Item>
          <Form.Item>
            <Button type='primary' htmlType='submit'>
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Contenedor>
    </div>
  );
}
