import React, { useEffect, useState } from 'react';
import Contenedor from '../components/inicio/container';
import { getOrganizations } from '../api/organizations';
import { Organization } from '../components/Interfaces';
import { OrganizationItem } from '../components/OngProfile/Organization/Item';
import { Row, Col, Button } from 'antd';
import { Link } from 'react-router-dom';

export function OrganizationTest() {
  const [organizations, setOrganizations] = useState<Organization[]>([]);
  const [isLoading, setIsloading] = useState<boolean>(true);

  useEffect(() => {
    getOrganizations().then(data => {
      setOrganizations(data);
      setIsloading(false);
      console.log(data);
    });
  }, []);

  return (
    <div className='Home fakeSpace'>
      <Contenedor>
        <div>
          <Link to={'/organizationsTestCreate'}>
            <Button shape='round' type='primary'>
              Agregar organization
            </Button>
          </Link>
        </div>
        <div>{isLoading && 'Cargando'}</div>
        <div>{!isLoading && organizations.length === 0 && 'No hay organizaciones'}</div>
        <div>
          <Row gutter={10}>
            {organizations.length > 0 &&
              organizations.map(organization => (
                <Col span={8}>
                  <OrganizationItem key={organization.organizationId} organization={organization} />
                </Col>
              ))}
          </Row>
        </div>
      </Contenedor>
    </div>
  );
}
