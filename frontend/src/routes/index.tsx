import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Layout from '../components/Layout';
import { Home } from '../pages/Home';
import OngProfile from '../pages/OngProfile';
import { OngList } from '../pages/OngList';

import '../less/Theme.less';
import { OrganizationTest } from '../pages/OrganizationsTest';
import { OrganizationTestOne } from '../pages/OrganizationsTestOne';
import { OrganizationTestCreate } from '../pages/OrganizationsTestCreate';
import Title from '../components/Title';

function Routes() {
  return (
    <Switch>
      <Layout>
        <Route
          exact
          path='/org/:slug'
          render={() => (
            <Title title='Organización'>
              <OngProfile />
            </Title>
          )}
        />
        <Route
          exact
          path='/projects'
          render={() => (
            <Title title='Proyectos'>
              <OngList />
            </Title>
          )}
        />
        <Route exact path='/organizationsTest' render={() => <OrganizationTest />} />
        <Route exact path='/organizationsTest/:slug' render={() => <OrganizationTestOne />} />
        <Route exact path='/organizationsTestCreate' render={() => <OrganizationTestCreate />} />
        <Route
          exact
          path='/'
          render={() => (
            <Title title='gentem | Directorio de proyectos que luchan contra el coronavirus '>
              <Home />
            </Title>
          )}
        />
      </Layout>
    </Switch>
  );
}

export { Routes };
